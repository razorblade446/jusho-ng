import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';
import {
  AccountWidgetComponent,
  SimpleHeaderLayoutComponent,
  SimpleHeaderComponent,
} from './components';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    SharedModule
  ],
  declarations: [
    AccountWidgetComponent,
    SimpleHeaderComponent,
    SimpleHeaderLayoutComponent
  ],
  exports: [
    SimpleHeaderLayoutComponent
  ]
})

export class JushoLayoutModule {
}
