import { Component, OnInit } from '@angular/core';
import { ConfigService } from '../../../core/services';

@Component({
  selector: 'simple-header',
  templateUrl: './simple-header.html',
  styleUrls: ['./simple-header.scss']
})

export class SimpleHeaderComponent implements OnInit {
  baseURL: any = '';

  constructor(private configService: ConfigService) {
  }

  ngOnInit() {
    this.baseURL = this.configService.globalConfiguration.baseURL;
  }
}
