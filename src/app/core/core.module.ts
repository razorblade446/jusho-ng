import { ModuleWithProviders, NgModule } from '@angular/core';
import { AuthenticationService, ConfigService } from './services';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { JwtInterceptor } from './interceptors/jwt.interceptor';
import { AdminAuthGuard, OperatorAuthGuard } from './guards';

@NgModule({
  imports: [
    HttpClientModule
  ],
})
export class CoreModule {
  public static forRoot(): ModuleWithProviders {
    return {
      ngModule: CoreModule,
      providers: [
        AuthenticationService,
        ConfigService
      ]
    };
  }
}
