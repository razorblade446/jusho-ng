import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class AdminAuthGuard implements CanActivate {

  constructor(private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const userData: any = localStorage.getItem('userData');

    if (!userData) {
      this.router.navigate(['/admin/login'], {queryParams: {returnUrl: state.url}});
      return false;
    }

    const userDataObject = JSON.parse(userData);
    const myDate = Math.floor(Date.now() / 1000);
    let myExpDate = 0;

    if (userDataObject && userDataObject.exp) {
      myExpDate = userDataObject.exp;
    }

    if (myExpDate < myDate) {
      this.router.navigate(['/admin/login'], {queryParams: {returnUrl: state.url}});
      return false;
    }

    if (userDataObject && userDataObject.scopes && userDataObject.scopes.indexOf('ROLE_ADMIN') !== -1) {
      return true;
    }

    this.router.navigate(['/forbidden']);
    return false;
  }

}
