import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class OperatorAuthGuard implements CanActivate {

  constructor(private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const userData: any = localStorage.getItem('userData');

    if (!userData) {
      this.router.navigate(['/login'], {queryParams: {returnUrl: state.url}});
      return false;
    }

    if (userData && userData.scopes && userData.scopes.indexOf('ROLE_OPERATOR') !== -1) {
      return true;
    }

    this.router.navigate(['/forbidden']);
    return false;
  }

}
