import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import { Router } from '@angular/router';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  constructor(private router: Router) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const userData = JSON.parse(localStorage.getItem('userData'));

    if (userData && userData.jwt) {
      req = req.clone({
        setHeaders: {
          Authorization: `Bearer ${userData.jwt}`
        }
      });
    }

    return next.handle(req).do(
      (event: HttpEvent<any>) => {
      },
      (error: HttpErrorResponse) => {
        if (error.status === 401) {
          if (this.router.url !== '/login') {
            this.router.navigate(['/login'], {queryParams: {returnUrl: this.router.url}});
          }
        }
      });
  }
}
