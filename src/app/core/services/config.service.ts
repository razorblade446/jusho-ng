import { Injectable } from '@angular/core';

@Injectable()
export class ConfigService {
  globalConfiguration: any = {};

  constructor() {
    this.globalConfiguration = require('assets/config.json') || {baseURL: 'test'};
  }

}
