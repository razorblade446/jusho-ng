import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { ConfigService } from './config.service';
import * as jwtDecode from 'jwt-decode';

@Injectable()
export class AuthenticationService {
  private baseURL = '';

  constructor(private http: HttpClient,
              private configService: ConfigService) {
    this.baseURL = configService.globalConfiguration.baseURL;
  }

  login(username: string, password): Observable<any> {
    return this.http.post<any>(`${this.baseURL}/login`, {login: username, password})
      .map(loginSuccess => {
        if (loginSuccess && loginSuccess.access_token) {
          this.storeUserDetails(loginSuccess.access_token);
        }

        return loginSuccess;
      });
  }

  logout() {
    localStorage.removeItem('userData');
  }

  private storeUserDetails(token) {
    const userData = jwtDecode(token);
    userData.jwt = token;
    localStorage.setItem('userData', JSON.stringify(userData));
  }
}
