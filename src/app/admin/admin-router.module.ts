import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminAuthGuard } from '@core/guards';
import {
  AdminDashboardLayoutComponent,
  AdminHomeComponent,
  AdminLoginComponent,
  ProductListComponent,
  CategoryListComponent,
  CategoryAddComponent,
  CategoryEditComponent,
  BrandEditComponent,
  BrandAddComponent,
  BrandListComponent,
  ProductEditComponent, BranchListComponent
} from '@admin/components';

const adminRoutes: Routes = [
  {
    path: 'login', component: AdminLoginComponent, pathMatch: 'full'
  },
  {
    path: '',
    component: AdminDashboardLayoutComponent,
    canActivate: [AdminAuthGuard],
    children: [
      {
        path: '', component: AdminHomeComponent, pathMatch: 'full'
      },
      {
        path: 'product/:id', component: ProductEditComponent
      },
      {
        path: 'product', component: ProductListComponent
      },
      {
        path: 'category/add', component: CategoryAddComponent
      },
      {
        path: 'category/:categoryId', component: CategoryEditComponent
      },
      {
        path: 'category', component: CategoryListComponent
      },
      {
        path: 'brand/add', component: BrandAddComponent
      },
      {
        path: 'brand/:brandId', component: BrandEditComponent
      },
      {
        path: 'brand', component: BrandListComponent
      },
      {
        path: 'branch', component: BranchListComponent
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(adminRoutes)
  ],
  exports: [
    RouterModule
  ]
})

export class AdminRouterModule {
}
