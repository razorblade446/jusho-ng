import { Component, OnInit } from '@angular/core';
import { BrandsService } from '@admin/services/brands.service';

@Component({
  selector: 'app-brand-list',
  templateUrl: './brand-list.component.html',
  styleUrls: ['./brand-list.component.scss']
})
export class BrandListComponent implements OnInit {
  brandList = [];

  constructor(private brandsService: BrandsService) { }

  ngOnInit() {
    this.brandsService.loadBrands()
      .subscribe((response: any) => {
        this.brandList.push(...response.content);
        // tslint:disable
        console.log('Brands Test:', this.brandList);
      });
  }

}
