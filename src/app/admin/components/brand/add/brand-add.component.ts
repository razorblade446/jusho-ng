import { Component, OnInit } from '@angular/core';
import { Brand } from '@admin/domain/brand';
import { Category } from '@admin/domain/category';
import { ActivatedRoute, Router } from '@angular/router';
import { BrandsService } from '@admin/services/brands.service';

@Component({
  selector: 'app-brand-add',
  templateUrl: './brand-add.component.html',
  styleUrls: ['./brand-add.component.scss']
})
export class BrandAddComponent implements OnInit {
  model: Brand;
  error: number;

  constructor(private brandsService: BrandsService,
              private router: Router,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.model = new Brand(null, null);
    this.error = 0;
  }
}
