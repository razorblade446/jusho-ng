import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../../../services/products.service';

@Component({
  selector: 'product-list',
  templateUrl: './product-list.html',
  styleUrls: ['./product-list.scss']
})

export class ProductListComponent implements OnInit {
  productList = [];

  constructor(private productsService: ProductsService) {
  }

  ngOnInit() {
    this.productsService.loadProducts()
      .subscribe((result: any) => {
        this.productList.push(...result.content);
        // tslint:disable
        console.log('Product List: ', this.productList);
      });
  }
}
