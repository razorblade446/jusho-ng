import { Component, OnInit } from '@angular/core';
import { ProductsService } from '@admin/services/products.service';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Product } from '@admin/domain/product';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'product-edit',
  templateUrl: './product-edit.component.html',
  styleUrls: ['./product-edit.component.scss']
})
export class ProductEditComponent implements OnInit {
  product$: Observable<Product>;
  product: Product;

  constructor(private route: ActivatedRoute,
              private productsService: ProductsService) {
  }

  ngOnInit() {
    this.product$ = this.route.paramMap
      .switchMap((params: ParamMap) => this.productsService.getProduct(params.get('id')));

    this.product$.subscribe((product) => this.product = product);
  }

}
