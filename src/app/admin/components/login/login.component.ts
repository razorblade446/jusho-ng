import { Component, OnInit } from '@angular/core';
import * as CryptoJS from 'crypto-js';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from '@core/services';

@Component({
  selector: 'admin-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class AdminLoginComponent implements OnInit {
  model: any = {};
  loading = false;
  returnUrl: string;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private authenticationService: AuthenticationService) {
  }

  ngOnInit() {
    this.authenticationService.logout();
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  login() {
    this.loading = true;
    this.authenticationService.login(this.model.username, this.model.password)
      .subscribe(loginSuccess => {
          this.router.navigate([this.returnUrl]);
        },
        error => {
          // tslint:disable-next-line
          console.log('Error:', error);
          this.loading = false;
        });
  }

  gravatarHash(email = '') {
    const gravatarUrl = 'https://www.gravatar.com/avatar/';
    const hash = CryptoJS.MD5(email.trim().toLowerCase());
    return gravatarUrl + hash;
  }

}
