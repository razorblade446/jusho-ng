import { Component, OnInit } from '@angular/core';
import { Category } from '../../../domain/category';
import { CategoriesService } from '../../../services/categories.service';
import { HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-category-add',
  templateUrl: './category-add.component.html',
  styleUrls: ['./category-add.component.scss']
})
export class CategoryAddComponent implements OnInit {
  model: Category;
  error: number;

  constructor(private categoriesService: CategoriesService,
              private router: Router,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.model = new Category(null, null);
    this.error = 0;
  }

  saveCategory() {
    this.categoriesService.createCategory(this.model)
      .subscribe(
        (response: any) => {
          this.router.navigate(['../'], {relativeTo: this.route});
        },
        (error: HttpErrorResponse) => {
          this.error = error.status;
        });
  }
}
