import { Component, OnInit } from '@angular/core';
import { CategoriesService } from '../../../services/categories.service';

@Component({
  selector: 'category-list',
  templateUrl: './category-list.html',
  styleUrls: ['./category-list.scss']
})

export class CategoryListComponent implements OnInit {
  categoryList = [];

  constructor(private categoriesService: CategoriesService) {
  }

  ngOnInit(): void {
    this.categoriesService.loadCategories()
      .subscribe((result: any) => {
        this.categoryList.push(...result.content);
        // tslint:disable
        console.log('Categories Test:', this.categoryList);
      });
  }
}
