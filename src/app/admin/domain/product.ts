import { Brand } from '@admin/domain/brand';
import { Category } from '@admin/domain/category';

export class Product {
  constructor(public id: number,
              public name: string,
              public description: string,
              public brand: Brand,
              public category: Category) {
  }
}
