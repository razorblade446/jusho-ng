import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from '../../core/services';
import { Observable } from 'rxjs/Observable';
import { Brand } from '../domain/brand';

@Injectable()

export class BranchesService {
  baseURL: string;

  constructor(private configService: ConfigService,
              private httpClient: HttpClient) {
    this.baseURL = configService.globalConfiguration.baseURL + '/admin/brand';
  }

  public loadBrands() {
    return this.httpClient.get(this.baseURL);
  }

  public createBrand(brand: Brand): Observable<Brand> {
    return this.httpClient.post<Brand>(this.baseURL, brand);
  }
}
