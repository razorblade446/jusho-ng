import { Injectable } from '@angular/core';
import { ConfigService } from '../../core/services';
import { IPageableRequest } from '../../core/common/utils';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Product } from '@admin/domain/product';

export interface IProductPageableRequest extends IPageableRequest {
  name?: string;
}

@Injectable()

export class ProductsService {
  private baseURL: string;

  constructor(private configService: ConfigService,
              private httpClient: HttpClient) {
    this.baseURL = configService.globalConfiguration.baseURL + '/admin/product';
  }

  public loadProducts(): Observable<any> {
    return this.httpClient.get(this.baseURL);
  }

  public getProduct(id: string): Observable<Product> {
    const productUrl = `${this.baseURL}/${id}`;
    return this.httpClient.get<Product>(productUrl);
  }
}
