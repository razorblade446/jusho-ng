import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from '../../core/services';
import { Category } from '../domain/category';
import { Observable } from 'rxjs/Observable';

@Injectable()

export class CategoriesService {
  baseURL: string;

  constructor(private configService: ConfigService,
              private httpClient: HttpClient) {
    this.baseURL = configService.globalConfiguration.baseURL + '/admin/category';
  }

  public loadCategories() {
    return this.httpClient.get(this.baseURL);
  }

  public createCategory(category: Category): Observable<any> {
    return this.httpClient.post(this.baseURL, category);
  }
}
