import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  AdminDashboardHeaderComponent,
  AdminDashboardLayoutComponent,
  AdminHomeComponent,
  AdminSidePanelComponent,
  AdminLoginComponent,
  ProductListComponent,
  ProductEditComponent,
  CategoryAddComponent,
  CategoryEditComponent,
  CategoryListComponent,
  BrandAddComponent,
  BrandEditComponent,
  BrandListComponent,
  BranchListComponent,
  PaginationComponent
} from '@admin/components';

import {
  BrandsService,
  CategoriesService,
  ProductsService
} from '@admin/services';

import { AdminRouterModule } from './admin-router.module';
import { SharedModule } from '@shared/shared.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { JwtInterceptor } from '@core/interceptors/jwt.interceptor';
import { FormsModule } from '@angular/forms';
import { AdminAuthGuard, OperatorAuthGuard } from '@core/guards';

@NgModule({
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    HttpClientModule,
    SharedModule,
    AdminRouterModule
  ],
  declarations: [
    CategoryListComponent,
    CategoryAddComponent,
    ProductListComponent,
    AdminHomeComponent,
    AdminDashboardHeaderComponent,
    AdminDashboardLayoutComponent,
    AdminSidePanelComponent,
    CategoryEditComponent,
    BrandListComponent,
    BrandAddComponent,
    BrandEditComponent,
    AdminLoginComponent,
    ProductEditComponent,
    BranchListComponent,
    PaginationComponent
  ],
  providers: [
    AdminAuthGuard,
    OperatorAuthGuard,
    CategoriesService,
    BrandsService,
    ProductsService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true
    }
  ]
})
export class AdminModule {
}
