import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@shared/shared.module';
import { StoreRouterModule } from './store-router.module';
import {
  StoreHeaderComponent,
  StoreLayoutComponent,
  StoreNavigationComponent,
  StoreHomeComponent,
  ProductTileComponent
} from '@store/components';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    StoreRouterModule
  ],
  declarations: [
    StoreLayoutComponent,
    StoreNavigationComponent,
    StoreHeaderComponent,
    StoreHomeComponent,
    ProductTileComponent
  ]
})
export class StoreModule {
}
