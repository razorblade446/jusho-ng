import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StoreLayoutComponent } from './components';
import { StoreHomeComponent } from '@store/components';

const storeRoutes: Routes = [
  {
    path: '',
    component: StoreLayoutComponent,
    children: [
      {
        path: '',
        component: StoreHomeComponent,
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
    imports: [
      RouterModule.forChild(storeRoutes)
    ],
    exports: [
      RouterModule
    ]
  }
)

export class StoreRouterModule {
}
