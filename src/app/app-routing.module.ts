import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { LoginComponent } from './components';
import { NotFoundPageComponent } from './components';

const appRoutes: Routes = [
  {
    path: '', loadChildren: './store/store.module#StoreModule'
  },
  {
    path: 'admin', loadChildren: './admin/admin.module#AdminModule'
  },
  {
    path: 'login', component: LoginComponent, pathMatch: 'full'
  },
  {
    path: 'not_found', component: NotFoundPageComponent, pathMatch: 'full'
  },
  {
    path: '**', redirectTo: '/not_found'
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes, {enableTracing: false})
  ],
  exports: [
    RouterModule
  ]
})

export class AppRoutingModule {
}
